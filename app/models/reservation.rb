class Reservation < ApplicationRecord
  belongs_to :book
  belongs_to :client
  belongs_to :librarian
  after_create :add_one
  after_destroy :destroy_one

  private
  def add_one
    self.book.update(stock: self.book.stock - 1)
  end
  def destroy_one
    self.book.update(stock: self.book.stock + 1)
  end
end
