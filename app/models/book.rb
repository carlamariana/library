class Book < ApplicationRecord
  belongs_to :author
  belongs_to :category
  has_many :reservations
  validates_presence_of :name, message: 'é um campo obrigatório'
  validates_presence_of :stock, message: 'é um campo obrigatório'
  validates_uniqueness_of :name, message: 'já foi cadastrado'
end
