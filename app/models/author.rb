class Author < ApplicationRecord
    has_many :books
    validates_presence_of :name, message: 'é um campo obrigatório'
    validates_uniqueness_of :name, message: 'já foi cadastrado'
end
