# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts "Gerando os clientes"
  5.times do |i|
    Client.create!(
      name: Faker::Name.name 
    )
  end
puts "OK"

puts "Gerando os autores"
  5.times do |i|
    Author.create!(
      name: Faker::Name.name 
    )
  end
puts "OK"

puts "Gerando as categorias"
  5.times do |i|
    Category.create!(
      name: Faker::Book.genre 
    )
  end
puts "OK"

puts "Gerando os librarians"
  2.times do |i|
    Librarian.create!(
      name: Faker::Name.name 
    )
  end
puts "OK"

puts "Gerando os librarians"
  1.times do |i|
    Librarian.create!(
      name: Faker::Internet.email
    )
  end
puts "OK"

puts "Gerando os livros"
  5.times do |i|
    Book.create!(
      name: Faker::Book.title,
      stock: Faker::Number.within(range: 1..5),
      author: Author.all.sample,
      category: Category.all.sample,
    )
  end
puts "OK"

puts "Gerando as reservas"
  5.times do |i|
    Reservation.create!(
      book: Book.all.sample,
      client: Client.all.sample,
      librarian: Librarian.all.sample,
    )
  end
puts "OK"